import gulp from 'gulp';
import sass from 'gulp-sass';
import browserSync from 'browser-sync';
import plumber from 'gulp-plumber';
import csscomb from 'gulp-csscomb';
import autoprefixer from 'gulp-autoprefixer';
import errorHandler from '../errorHandler';

gulp.task('styles', () => {
	return gulp
		.src('app/styles/*.sass')
		.pipe(plumber({errorHandler}))
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false,
		}))
		.pipe(csscomb())
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.stream());
});

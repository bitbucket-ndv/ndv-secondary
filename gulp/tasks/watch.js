import gulp from 'gulp';

gulp.task('watch', () => {
	global.watching = true;

	gulp.watch('app/scripts/**/*.js', ['scripts']);

	gulp.watch('app/templates/**/*.html', ['templates']);

	gulp.watch('app/styles/**/*.sass', ['styles']);
});
